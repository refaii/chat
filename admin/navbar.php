

<nav class="navbar navbar-default" >
    <div class="container-fluid" style='background:#3c5a99db;' >
		
		
		<ul class="nav navbar-nav" >
			<li><a href="index.php" style='color:#fff'><span class="glyphicon glyphicon-list"></span> discussions</a></li>
			<li><a href="user.php" style='color:#fff'><span class="glyphicon glyphicon-user"></span> Utilisateurs</a></li>
		</ul>
		
		<ul class="nav navbar-nav navbar-right" >
			<li><a href="#account" style='color:#fff' data-toggle="modal"><span class="glyphicon glyphicon-lock"></span> <?php echo $user; ?></a></li>
			<li class="dropdown" style='color:#fff'>
				<a class="dropdown-toggle" data-toggle="dropdown" href="#" style='color:#fff;background:#3c5a9908;' ><span class="caret"></span></a>
                    <ul class="dropdown-menu" style='background-color:#fff'>
						<li><a href="#photo" style='color:#000' data-toggle="modal"><span class="glyphicon glyphicon-picture"></span> Ajouter photo</a></li>
						<li class="divider"></li>
                        <li><a href="#logout" style='color:#000' data-toggle="modal"><span class="glyphicon glyphicon-log-out"></span> Se déconnecter</a></li>
                    </ul>
			</li>
		</ul> 
    </div>
</nav>