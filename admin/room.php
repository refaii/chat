		
		<head>
		<link rel="stylesheet" href="../emogi/emojionearea.min.css">
		<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

					<script type="text/javascript" src="../emogi/emojionearea.min.js"></script>
		</head>
		
		<div class="col-lg-9">
            <div class="panel panel-default " style="height:50px;">
				<span style="font-size:18px; margin-left:10px; position:relative; top:13px;"><strong>Salle de clavardage : <?php echo $chatrow['chat_name']; ?></strong></span>
				<div class="pull-right " style="margin-right:10px; margin-top:7px;">
					<span id="user_details" style="font-size:18px; position:relative; top:2px;"><strong>membres: </strong><span class="badge"><?php echo mysqli_num_rows($cmem); ?></span></span>
					<a href="#add_member" data-toggle="modal" class="btn btn-primary">Ajouter un membre</a>
					<a href="#delete_room" data-toggle="modal" class="btn btn-danger">Supprimer la salle</a>
					<a href="index.php" class="btn btn-primary" style="background:#d58512;border-color:#d58512"><span class="glyphicon glyphicon-arrow-left"></span> Retour</a>
				</div>
				
				<div class="showme hidden" style="position: absolute; left:570px; top:20px;">
					<div class="well">
						<strong>Membre/s de la salle:</strong> </br>
						
					<?php
						$rm=mysqli_query($conn,"select * from chat_member left join `user` on user.userid=chat_member.userid where chatroomid='$id'");
						while($rmrow=mysqli_fetch_array($rm)){
							?>
							<span>
							
							<?php
								$creq=mysqli_query($conn,"select * from chatroom where chatroomid='$id'");
								$crerow=mysqli_fetch_array($creq);
								
								if ($crerow['userid']==$rmrow['userid']){
									?>
										<span class="glyphicon glyphicon-user"></span>
									<?php
								}
								
							?>
							
							<?php  echo  $rmrow['uname']  ; ?></span><br>
							
							<?php
						}
						
					?>
						
					</div>
				</div>
			</div>
			<div>
				<div class="panel panel-default" style="height: 400px;background:rgb(247 247 247 / 40%);">
				<div style="background:#fff">
					<div style="height:10px;"></div>
					
					<span style="margin-left:10px;">Bienvenue à Chatroom</span><br>
					<span style="font-size:10px; margin-left:10px;color:red"><i>Remarque: évitez d'utiliser un langage grossier et des discours haineux pour éviter l'interdiction de compte.</i></span>
					<div style="height:10px;"></div>
					</div>
					<div id="chat_area" style="margin-left:10px; max-height:320px; overflow-y:scroll;">
					</div>
				</div>
				
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Saisir un message..." id="chat_msg">
					<span class="input-group-btn">
					<button class="btn btn-success" type="submit" id="send_msg" value="<?php echo $id; ?>">
					<span class="glyphicon glyphicon-comment"></span> envoyer
					</button>
					</span>
				</div>
				
			</div>			
		</div>
		
		<script >
          
			$("#chat_msg").emojioneArea();
			
		
		</script>

		<div class="col-lg-3" onload = "JavaScript:AutoRefresh(5000);" style="height:400px;overflow-y:auto;top:55px">
		<caption><strong>Membre/s de la salle:</strong></caption>
   <table width="50%" class="table table-striped table-bordered table-hover" id="userList" style="height:200px;">
	   <thead>
		   <tr>
			   
		   </tr>
	   </thead>
	   <tbody>
	   <?php
	   
	  
		   $query=mysqli_query($conn,"select * from chat_member left join `user` on user.userid=chat_member.userid where chatroomid='$id'");
		   while($row=mysqli_fetch_array($query)){
		   ?>
		   <tr >
			   <td > <img src="../<?php if(empty($row['photo'])){echo "upload/profile.jpg";}else{echo $row['photo'];} ?>" height="30px;" width="30px;" style='border-radius:50%;margin-right:8px;'><input  type="hidden" id="ename<?php echo $row['userid']; ?>" value="<?php echo $row['uname']; ?>"> <?php echo $row['uname'];  ?></td>
			   
			   
			   <td> 
				   
				   <?php
				   if($row['access'] !=1){ 

				   if ($row['bloque'] == 1){
					   
					?>
					 <button type="button" class="btn btn-danger desblock" style='background:rgb(22 169 34 / 71%);border-color:rgb(22 169 34 / 71%);' value="<?php echo $row['userid']; ?>"><span> <img src="unlocked.png" style="width:16px; color:#fff"></span> debloquer </button>
					
					<?php
					
				   } else{
						
					   ?>
					   
					    <button type="button" class="btn btn-danger block" value="<?php echo $row['userid']; ?>"><span> <img src="blocked-01.png"  style="color:#fff;width:16px"></span> bloquer  </button>
				   <?php
				   
				   }
			   }
				   ?>
				   
			   </td>
		   </tr>
		   <?php
		   }
	   ?>
	   </tbody>
   </table>                     
</div>
<script>

$(document).on('click', '.block', function AutoRefresh(){
		var nrid=$(this).val();
		
			$.ajax({	
				url:"blockuser.php",
				method:"POST",
				data:{
					id: nrid,
					del: 1,				
				},
				success:function(t){
					//window.location.href='chatroom.php';
					setTimeout("location.reload(true);", t);			
				}
			});
	});
	
	$(document).on('click', '.desblock', function AutoRefresh(){
		
		var nrid=$(this).val();
		
			$.ajax({
				
				url:"desblock.php",
				method:"POST",
				data:{
					id: nrid,
					del: 1,
		
				},
				success:function(t){
					//window.location.href='chatroom.php';
					setTimeout("location.reload(true);", t);
				}
			});
	});

</script>