<div class="col-lg-12">
    <div class="panel panel-default" style="height:50px;">
		<span style="font-size:18px; margin-left:10px; position:relative; top:13px;"><strong><span class="glyphicon glyphicon-user"></span> User List</strong></span>
		<div class="pull-right" style="margin-right:10px; margin-top:7px;">
			<a href="#add_user" data-toggle="modal" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> ajouter</a>
		</div>
	</div>
	<table width="100%" class="table table-striped table-bordered table-hover" id="userList">
        <thead>
            <tr>
                <th>Name</th>
				<th>Username</th>
				<th>Password</th>
				<th>Photo</th>
				<th>Access</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		<?php
		
		
			$query=mysqli_query($conn,"select * from `user` order by uname asc");
			while($row=mysqli_fetch_array($query)){
			?>
			<tr>
				<td><input type="hidden" id="ename<?php echo $row['userid']; ?>" value="<?php echo $row['uname']; ?>"><?php echo $row['uname']; ?></td>
				<td><input type="hidden" id="eusername<?php echo $row['userid']; ?>" value="<?php echo $row['username']; ?>"><?php echo $row['username']; ?></td>
				<td><input type="hidden" id="epassword<?php echo $row['userid']; ?>" value="<?php echo $row['password']; ?>"><?php echo $row['password']; ?></td>
				<td><img src="../<?php if(empty($row['photo'])){echo "upload/profile.jpg";}else{echo $row['photo'];} ?>" height="30px;" width="30px;"></td>
				<td>
					<?php 
						if ($row['access']==1){
							echo "Admin";
						}
						if ($row['access']==2){
							echo "utilisateur";
						}
						if ($row['access']==3){
							echo "Moderator";
						}
						
					?>
					
				</td>
				<td> 
					<button type="button" class="btn btn-warning edituser" value="<?php echo $row['userid']; ?>"><span class="glyphicon glyphicon-pencil"></span> modifier</button>  ||
					<button type="button" class="btn btn-danger deleteuser" value="<?php echo $row['userid']; ?>"><span class="glyphicon glyphicon-trash"></span> supprimer</button> 
					<?php
					if($row['access'] !=1){ 

					if ($row['bloque'] == 1){
						
                     ?>
					 || <button type="button" class="btn btn-danger desblock" style='background:rgb(22 169 34 / 71%);border-color:rgb(22 169 34 / 71%);' value="<?php echo $row['userid']; ?>"><span> <img src="unlocked.png" style="width:16px; color:#fff"></span> debloquer </button>
					 
					 <?php
					 
					} else{
						
						?>
						
						|| <button type="button" class="btn btn-danger block" value="<?php echo $row['userid']; ?>"><span> <img src="blocked-01.png"  style="color:#fff;width:16px"></span> bloquer  </button>
					<?php
					
					}
				}
					?>
					
				</td>
			</tr>
			<?php
			}
		?>
        </tbody>
    </table>                     
</div>

<script>
$(document).on('click', '.block', function(){
	var nrid=$(this).val();
	
		$.ajax({	
			url:"blockuser.php",
			method:"POST",
			data:{
				id: nrid,
				del: 1,				
			},
			success:function(){
				window.location.href='user.php';			
			}
		});
});

$(document).on('click', '.desblock', function(){
	
	var nrid=$(this).val();
	
		$.ajax({
			
			url:"desblock.php",
			method:"POST",
			data:{
				id: nrid,
				del: 1,
	
			},
			success:function(){
				window.location.href='user.php';
			}
		});
});
</script>